Experiments with Rust on the ESP32 CYD

Details of the board:
- https://github.com/witnessmenow/ESP32-Cheap-Yellow-Display

Doc:
- https://esp-rs.github.io/book/
- https://esp-rs.github.io/esp-idf-hal
- https://docs.rs/embedded-graphics/
- https://docs.rs/mipidsi/
- https://github.com/esp-rs/awesome-esp-rust

Articles:
- https://lilymara.xyz/posts/images-esp32/
- https://nereux.blog/posts/getting-started-esp32-nostd/

